//********************************************************************* 
//*                                                                   * 
//*   CIS611             Spring 2018 Luis Castro and  Rocco Meconi    * 
//*                                                                   * 
//*                      Program Assignment PP03                      * 
//*                                                                   * 
//*                      Class Description                            * 
//*                      Date Created    03/08/18                     * 
//*                                                                   * 
//*                      Saved in: TaxIncome.java                     * 
//*                                                                   * 
//*********************************************************************
package PP03;

public class TaxIncome implements Taxable{
	// 1- this class implements the Taxable interface
	
	
	// 2- implement all the unimplemented abstract methods in the Taxable Interface, income tax is computed based on state and federal taxes   
	
	@Override
	public double compStateTax(double grossPay) {
		double stateTax = grossPay * Taxable.STATE_TAX;
		return stateTax;
	}

	@Override
	public double compFederalTax(double grossPay) {
		double federalTax = grossPay * Taxable.FEDERAL_TAX;
		return federalTax;
	}

	@Override
	public double compIncomeTax(double grossPay) {
		double incomeTax = (compFederalTax(grossPay) + compStateTax(grossPay));
		return incomeTax;
	}

	
	
	

}
