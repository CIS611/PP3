//********************************************************************* 
//*                                                                   * 
//*   CIS611             Spring 2018 Luis Castro and  Rocco Meconi    * 
//*                                                                   * 
//*                      Program Assignment PP03                      * 
//*                                                                   * 
//*                      Class Description                            * 
//*                      Date Created    03/08/18                     * 
//*                                                                   * 
//*                      Saved in: PayRecord.java                     * 
//*                                                                   * 
//*********************************************************************
package PP03;

public class PayRecord {

	private int rID;
	private Employee employee;
	private PayPeriod payPeriod;
	private TaxIncome payTax;

	private double payHours;
	private double payRate;

	private double montlyIncome;
	private int numMonths;

	public static final int REG_HOURS = 40;
	public static final double OT_RATE = 1.25;

	// pay record constructor for hourly employee
	public PayRecord(int id, Employee e, PayPeriod period, double hours, double rate) {

		this.rID = id;
		this.employee = e;
		this.payPeriod = period;
		this.payHours = hours;
		this.payRate = rate;
		this.montlyIncome = 0;
		this.numMonths = 0;
		this.payTax = new TaxIncome();

	}

	// pay record constructor for full time employee
	public PayRecord(int id, Employee e, PayPeriod period, double mIncome, int mNum) {

		this.rID = id;
		this.employee = e;
		this.payPeriod = period;
		this.payHours = 0;
		this.payRate = 0;
		this.montlyIncome = mIncome;
		this.numMonths = mNum;
		this.payTax = new TaxIncome();

	}

	// 1- add setters and getters methods
	public int getrID() {
		return rID;
	}

	public void setrID(int rID) {
		this.rID = rID;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public PayPeriod getPayPeriod() {
		return payPeriod;
	}

	public void setPayPeriod(PayPeriod payPeriod) {
		this.payPeriod = payPeriod;
	}

	public TaxIncome getPayTax() {
		return payTax;
	}

	public void setPayTax(TaxIncome payTax) {
		this.payTax = payTax;
	}

	public double getPayHours() {
		return payHours;
	}

	public void setPayHours(double payHours) {
		this.payHours = payHours;
	}

	public double getPayRate() {
		return payRate;
	}

	public void setPayRate(double payRate) {
		this.payRate = payRate;
	}

	public double getMontlyIncome() {
		return montlyIncome;
	}

	public void setMontlyIncome(double montlyIncome) {
		this.montlyIncome = montlyIncome;
	}

	public int getNumMonths() {
		return numMonths;
	}

	public void setNumMonths(int numMonths) {
		this.numMonths = numMonths;
	}

	// 3- complete the code in the following methods: grossPay() and netPay()
	// complete the code to compute the gross pay for the employee based on the
	// employee status
	public double grossPay() {
		double grossPay = 0.0;
		if (this.employee.getEmpStatus().equals(Status.FullTime)) {
			grossPay = this.numMonths * this.montlyIncome;
		}
		
		if (this.employee.getEmpStatus().equals(Status.Hourly)) {
			if (this.payHours <= REG_HOURS) {
				grossPay = this.payHours * this.payRate;
			} else { 
				double regGrossPay = REG_HOURS * this.payRate;
				double extraHours = this.payHours - REG_HOURS;
				double extraGrossPay = extraHours * OT_RATE;
				grossPay = regGrossPay + extraGrossPay;
			}
		}
		return grossPay;
	}

	// complete the code in this method to compute the net pay of the employee after
	// taxes (state and federal)
	public double netPay() {
		return this.grossPay() - this.getPayTax().compIncomeTax(this.grossPay());
	}

	// 2- add override method toString()
	@Override
	public String toString() {
		return "PayRecord [rID=" + rID + ", employee=" + employee + ", payPeriod=" + payPeriod + ", payHours="
				+ payHours + ", payRate=" + payRate + ", montlyIncome=" + montlyIncome + ", numMonths=" + numMonths
				+ ", grossPay()=" + this.grossPay() + ", netPay()=" + this.netPay() + "]";
	}

}
