//********************************************************************* 
//*                                                                   * 
//*   CIS611             Spring 2018 Luis Castro and  Rocco Meconi    * 
//*                                                                   * 
//*                      Program Assignment PP03                      * 
//*                                                                   * 
//*                      Class Description                            * 
//*                      Date Created    03/08/18                     * 
//*                                                                   * 
//*                      Saved in: PayPeriod.java                     * 
//*                                                                   * 
//*********************************************************************
package PP03;

import java.time.LocalDate;


public class PayPeriod {
	
	private int pID;
    private LocalDate pStartDate, pEndDate;
    
 // 1- add the class constructor
	public PayPeriod(int pID, LocalDate pStartDate, LocalDate pEndDate) {
		super();
		this.pID = pID;
		this.pStartDate = pStartDate;
		this.pEndDate = pEndDate;
	} 	
    // 2- add the setters/getters methods

	public int getpID() {
		return pID;
	}

	public void setpID(int pID) {
		this.pID = pID;
	}

	public LocalDate getpStartDate() {
		return pStartDate;
	}

	public void setpStartDate(LocalDate pStartDate) {
		this.pStartDate = pStartDate;
	}

	public LocalDate getpEndDate() {
		return pEndDate;
	}

	public void setpEndDate(LocalDate pEndDate) {
		this.pEndDate = pEndDate;
	}

	// 3- add override method toString() 
	@Override
	public String toString() {
		return "PayPeriod [pID=" + pID + ", pStartDate=" + pStartDate + ", pEndDate=" + pEndDate + "]";
	}
	
    
    

	
}
