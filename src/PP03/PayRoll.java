//********************************************************************* 
//*                                                                   * 
//*   CIS611             Spring 2018 Luis Castro and  Rocco Meconi    * 
//*                                                                   * 
//*                      Program Assignment PP03                      * 
//*                                                                   * 
//*                      Class Description                            * 
//*                      Date Created    03/08/18                     * 
//*                                                                   * 
//*                      Saved in: PayRoll.java                       * 
//*                                                                   * 
//*********************************************************************
package PP03;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Scanner;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.LineNumberReader;
import java.nio.file.Paths;

import javax.swing.JTextArea;

public class PayRoll {

	private String fileName;
	private int n;
	private PayRecord[] payRecords;
	private double totalNetPay;
	private double avgNetPay;
	private int index;

	public PayRoll() {
	}

	public PayRoll(String fileName, int n) {
		this.fileName = fileName;
		this.n = n;
	}

	public void readFromFile() throws Exception {
		Employee employee = null;
		HashMap<String, Employee> employeeHash = new HashMap<String, Employee>();

		LineNumberReader lineReader = new LineNumberReader(new FileReader(Paths.get(fileName).toFile()));
		lineReader.skip(Long.MAX_VALUE);
		int totalNumberOfLines = lineReader.getLineNumber() + 1;
		lineReader.close();

		// Create a Scanner for the file
		File file = new File(fileName);
		Scanner input = new Scanner(file).useDelimiter(",|\\n");

		payRecords = new PayRecord[((totalNumberOfLines - 1) / 2) + n];

		// Read fields from the file
		while (input.hasNext()) {
			String recordType = input.next();
			if (recordType.equals("employee")) {
				String empId = input.next().trim();
				String firstName = input.next().trim();
				String lastName = input.next().trim();
				String empStatus = input.next().trim();
				String street = input.next().trim();
				String houseNumber = input.next().trim();
				String city = input.next().trim();
				String state = input.next().trim();
				String zipCode = input.next().trim();

				employee = this.createEmployee(empId, empStatus, street, houseNumber, city, state, zipCode, firstName,
						lastName);
				employeeHash.put(empId, employee);
			}

			if (recordType.equals("payRecord")) {
				String payRecordId = input.next().trim();
				String payRecordEmpId = input.next().trim();

				String payRecordPayMonth = "";
				String payRecordPayHour = "";
				String payRecordPayMonthHour = input.next().trim();
				if (payRecordPayMonthHour.contains("<m>")) {
					payRecordPayMonth = payRecordPayMonthHour.split("<")[0];
				}

				if (payRecordPayMonthHour.contains("<h>")) {
					payRecordPayHour = payRecordPayMonthHour.split("<")[0];
				}

				String payRecordNumMonths = "";
				String payRecordRate = "";
				String payRecordRateMonth = input.next().trim();
				if (payRecordRateMonth.contains("<n>")) {
					payRecordNumMonths = payRecordRateMonth.split("<")[0];
				}

				if (payRecordRateMonth.contains("<r>")) {
					payRecordRate = payRecordRateMonth.split("<")[0];
				}

				String payRecordPeriodId = input.next().trim();
				String payRecordStartPayPeriod = input.next().trim();
				String payRecordEndPayPeriod = input.next().trim();

				this.createPayRecord(employeeHash, payRecordEmpId, payRecordId, payRecordPayHour, payRecordRate,
						payRecordPayMonth, payRecordNumMonths, payRecordPeriodId, payRecordStartPayPeriod,
						payRecordEndPayPeriod);

				index++;
			}
		}
	}

	// TODO: add address and employee fields
	public void writeToFile() throws Exception {
		File file = new File("PayRecord.txt");
		// creates the file
		file.createNewFile();
		// creates a FileWriter Object
		FileWriter writer = new FileWriter(file);
		// Writes the content to the file
		for (int i = 0; i < payRecords.length; i++) {
			if (payRecords[i] != null) {
				writer.write(payRecords[i].getrID() + ", " + payRecords[i].getEmployee().geteID() + ", "
						+ payRecords[i].getEmployee().getEmpStatus() + ", " + payRecords[i].getEmployee().getfName()
						+ ", " + payRecords[i].getEmployee().getlName() + ", "
						+ payRecords[i].getEmployee().getAddress().getStreet() + ", "
						+ payRecords[i].getEmployee().getAddress().getHouseNumber() + ", "
						+ payRecords[i].getEmployee().getAddress().getCity() + ", "
						+ payRecords[i].getEmployee().getAddress().getState() + ", "
						+ payRecords[i].getEmployee().getAddress().getZipCode() + ", "
						+ payRecords[i].getPayPeriod().getpID() + ", " + payRecords[i].getPayPeriod().getpStartDate()
						+ ", " + payRecords[i].getPayPeriod().getpEndDate() + ", " + payRecords[i].getPayHours() + ", "
						+ payRecords[i].getPayRate() + ", " + payRecords[i].getMontlyIncome() + ", "
						+ payRecords[i].getNumMonths() + ", " + payRecords[i].grossPay() + ", " + payRecords[i].netPay()
						+ "\n");
			}
		}
		writer.flush();
		writer.close();
	}

	public Employee createEmployee(String empId, String empStatus, String street, String houseNumber, String city,
			String state, String zipCode, String firstName, String lastName) {
		Address address = new Address(street, Integer.parseInt(houseNumber), city, state, Integer.parseInt(zipCode));
		Employee employee = new Employee(firstName, lastName, address);
		employee.seteID(Integer.parseInt(empId));
		if (empStatus.equals("FULLTIME")) {
			employee.setEmpStatus(Status.FullTime);
		}
		if (empStatus.equals("HOURLY")) {
			employee.setEmpStatus(Status.Hourly);
		}
		return employee;
	}

	public Employee createEmployee(int empId, String empStatus, String street, int houseNumber, String city,
			String state, int zipCode, String firstName, String lastName) {
		Address address = new Address(street, houseNumber, city, state, zipCode);
		Employee employee = new Employee(firstName, lastName, address);
		employee.seteID(empId);
		if (empStatus.equals("FULLTIME")) {
			employee.setEmpStatus(Status.FullTime);
		}
		if (empStatus.equals("HOURLY")) {
			employee.setEmpStatus(Status.Hourly);
		}
		return employee;
	}
	
	public void createPayRecord(HashMap<String, Employee> employeeHash, String empID, String rID, String hours,
			String rate, String mIncome, String mNum, String periodId, String startPeriod, String endPeriod) {
		Employee employee = employeeHash.get(empID);
		PayPeriod payPeriod = new PayPeriod(Integer.parseInt(periodId), convertStringToDate(startPeriod),
				convertStringToDate(endPeriod));
		if (employee.getEmpStatus().equals(Status.Hourly)) {
			PayRecord payRecord = new PayRecord(Integer.parseInt(rID), employee, payPeriod, Double.parseDouble(hours),
					Double.parseDouble(rate));
			payRecords[index] = payRecord;
		}

		if (employee.getEmpStatus().equals(Status.FullTime)) {
			PayRecord payRecord = new PayRecord(Integer.parseInt(rID), employee, payPeriod, Double.parseDouble(mIncome),
					Integer.parseInt(mNum));
			payRecords[index] = payRecord;
		}

	}

	public void createPayRecord(Employee employee, int rID, double hours, double rate, double mIncome, int mNum,
			int periodId, LocalDate startPeriod, LocalDate endPeriod) {
		PayPeriod payPeriod = new PayPeriod(periodId, startPeriod, endPeriod);
		if (employee.getEmpStatus().equals(Status.Hourly)) {
			PayRecord payRecord = new PayRecord(rID, employee, payPeriod, hours, rate);
			payRecords[index] = payRecord;
			index++;
		}

		if (employee.getEmpStatus().equals(Status.FullTime)) {
			PayRecord payRecord = new PayRecord(rID, employee, payPeriod, mIncome, mNum);
			payRecords[index] = payRecord;
			index++;
		}
	}
	
	//TODO: this.totalNetPay and this.avgNetPay are 0 - check why
	public void displayPayRecord(JTextArea textArea) {
		textArea.setText(null);
		for (int i = 0; i < payRecords.length; i++) {
			if (payRecords[i] != null) {
				textArea.append(payRecords[i].toString());
				textArea.append("\n");
			}
		}
		textArea.append("-----------------------------------------------------------------");
		textArea.append("\n");
		textArea.append("[Total Average Net Pay = " + this.avgNetPay() + "] "); 
		textArea.append("[Total Net Pay = " + this.totalNetPay + "]");
		textArea.append("\n");
	}
	

	public double avgNetPay() {
		double totalNetPayAvg = 0.0;
		double totalNetPay = 0.0;
		for (int i = 0; i < payRecords.length - 1; i++) {
			if (payRecords[i] != null) {
				totalNetPay = totalNetPay + payRecords[i].netPay();
			}
		}
		totalNetPayAvg = totalNetPay / payRecords.length;

		this.totalNetPay = totalNetPay;
		this.avgNetPay = totalNetPayAvg;

		return totalNetPayAvg;
	}

	private LocalDate convertStringToDate(String strDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		return LocalDate.parse(strDate, formatter);
	}

}
