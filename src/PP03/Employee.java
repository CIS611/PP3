//********************************************************************* 
//*                                                                   * 
//*   CIS611             Spring 2018 Luis Castro and  Rocco Meconi    * 
//*                                                                   * 
//*                      Program Assignment PP03                      * 
//*                                                                   * 
//*                      Class Description                            * 
//*                      Date Created    03/08/18                     * 
//*                                                                   * 
//*                      Saved in: Employee.java                      * 
//*                                                                   * 
//*********************************************************************
package PP03;


public class Employee extends Person {
	
	private int eID;
    private Status empStatus;
    
    // 1- The Employee class extends superclass Person
    // 2- add the subclass Employee constructor that calls the supper Person class constructor, you should provide input data for all parent class data fields
    	Employee(String fName, String lName, Address address){
    		super(fName, lName, address);
    		this.empStatus = empStatus;
    		
    	}
   // 3- add setters/getters methods

		public int geteID() {
			return eID;
		}

		public void seteID(int eID) {
			this.eID = eID;
		}

		public Status getEmpStatus() {
			return empStatus;
		}

		public void setEmpStatus(Status empStatus) {
			this.empStatus = empStatus;
		}

		// 4- add override toString() method that overrides toString() in the superclass Person 
		@Override
		public String toString() {
			return "Employee [eID=" + eID + ", empStatus=" + empStatus + ", " + "Person [fName=" + fName + ", lName=" + lName + ", address=" + address + "]";
		}
    	
   
		
	
}
