//********************************************************************* 
//*                                                                   * 
//*   CIS611             Spring 2018 Luis Castro and  Rocco Meconi    * 
//*                                                                   * 
//*                      Program Assignment PP03                      * 
//*                                                                   * 
//*                      Class Description                            * 
//*                      Date Created    03/08/18                     * 
//*                                                                   * 
//*                      Saved in: UserGUI.java                       * 
//*                                                                   * 
//*********************************************************************
package PP03;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;

public class UserGUI extends JPanel {
	private JLabel empLabel;
	private JLabel empIdLabel;
	private JLabel empFirstNameLabel;
	private JLabel empLastNameLabel;
	private JLabel empStatusLabel;
	private JLabel empAddressLabel;
	private JLabel empStreetLabel;
	private JLabel empHouseNumLabel;
	private JLabel empCityLabel;
	private JLabel empStateLabel;
	private JLabel empZipCodeLabel;
	private JLabel payPeriodIdLabel;
	private JLabel payPeriodStartDateLabel;
	private JLabel payPeriodEndDateLabel;
	private JLabel payPeriodLabel;
	private JLabel payRecordLabel;
	private JLabel payRecordMonthIncomeLabel;
	private JLabel payRecordNumMonthsLabel;
	private JLabel payRecordIdLabel;
	private JLabel payRecordPayHoursLabel;
	private JLabel payRecordPayRate;
	private JLabel empRecStatsLabel;
	private JTextField empIdText;
	private JTextField empFirstNameText;
	private JTextField empLastNameText;
	private JTextField empStreetText;
	private JTextField empHouseNumText;
	private JTextField empCityText;
	private JTextField empZipCodeText;
	private JTextField payPeriodIdText;
	private JTextField payPeriodStartDateText;
	private JTextField payPeriodEndDateText;
	private JTextField payRecordMonthIncomeText;
	private JTextField payRecordNumMonthsText;
	private JTextField payRecordIdText;
	private JTextField payRecordPayHoursText;
	private JTextField payRecordPayRateText;
	private JButton AddEmployeeButton;
	private JButton AddPayRecButton;
	private JButton CloseButton;
	private JComboBox statesCombList;
	private JScrollPane jp;
	private JRadioButton fullTimeRadioButton = new JRadioButton("FULLTIME");
	private JRadioButton hourlyRadioButton = new JRadioButton("HOURLY");
	private ButtonGroup buttonGroup = new ButtonGroup();

	private int empId;
	private String empFirstName;
	private String empLastName;
	private String empStatus;
	private String empStreet;
	private int empHouseNum;
	private String empCity;
	private String empState;
	private int empZipCode;
	private int payPeriodId;
	private LocalDate payPeriodStartDate;
	private LocalDate payPeriodEndDate;
	private int payRecId;
	private double payRecMonthIncome;
	private int payRecNumMonths;
	private double payRecPayHours;
	private double payRecPayRate;

	static String numEmp;
	static int n;
	static int empCount = 0;
	static int payRecCount = 0;
	static PayRoll payRoll;
	static Employee employee;
	static PayRecord payRecord;
	static String fileName = "PayRoll.txt";
	static JTextArea textArea;

	public UserGUI() throws Exception {

		initializeGUI();
		renderLayout();

		AddEmployeeButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					if (empCount == n) {
						String message = "You have completed the number of employees provided - click the close button to exit.";
						JOptionPane.showMessageDialog(null, message);
						return;
					} else {
						createEmployee();
					}

				} catch (Exception ex) {
					String message = "An error occurred during employee insertion - the pay roll system will exit.";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
					System.exit(0);
				}
			}
		});

		AddPayRecButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (payRecCount == n) {
						String message = "You have completed the number of employees provided - click the close button to exit.";
						JOptionPane.showMessageDialog(null, message);
						return;
					} else {
						createPayRecord();
					}

				} catch (Exception ex) {
					String message = "An error occurred during pay record insertion - the pay roll system will exit.";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
					System.exit(0);
				}

			}
		});

		CloseButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					closeApplication();
				} catch (Exception ex) {
					String message = "An error occurred while writing the records to the output file - the pay roll system will exit.";
					JOptionPane.showMessageDialog(null, message);
					ex.printStackTrace();
					System.exit(0);
				}
			}
		});

		fullTimeRadioButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				payRecordPayHoursText.setEditable(false);
				payRecordPayRateText.setEditable(false);
				payRecordMonthIncomeText.setEditable(true);
				payRecordNumMonthsText.setEnabled(true);
				payRecordNumMonthsText.setOpaque(true);

			}
		});

		hourlyRadioButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				payRecordPayHoursText.setEditable(true);
				payRecordPayRateText.setEditable(true);
				payRecordMonthIncomeText.setEditable(false);
				payRecordNumMonthsText.setEnabled(false);
				payRecordNumMonthsText.setOpaque(false);
			}
		});

	} // end of constructor

	static void processPayRollFile() throws Exception {
		payRoll = new PayRoll(fileName, n);
		payRoll.readFromFile();
		payRoll.displayPayRecord(textArea);
	}

	private void initializeGUI() {
		empLabel = new JLabel("Employee:");
		empIdLabel = new JLabel("ID:");
		empFirstNameLabel = new JLabel("First Name:");
		empLastNameLabel = new JLabel("Last Name:");
		empStatusLabel = new JLabel("Employee Status:");
		empAddressLabel = new JLabel("Employee Address:");
		empStreetLabel = new JLabel("Street:");
		empHouseNumLabel = new JLabel("H/Apt Number:");
		empCityLabel = new JLabel("City:");
		empStateLabel = new JLabel("State:");
		empZipCodeLabel = new JLabel("Zip Code:");
		payPeriodIdLabel = new JLabel("ID:");
		payPeriodStartDateLabel = new JLabel("Start Date (mm/dd/yyyy):");
		payPeriodEndDateLabel = new JLabel("End Date (mm/dd/yyyy):");
		payPeriodLabel = new JLabel("Pay Period:");
		payRecordLabel = new JLabel("Pay Record:");
		payRecordMonthIncomeLabel = new JLabel("Monthly Income:");
		payRecordNumMonthsLabel = new JLabel("Number of Months:");
		payRecordIdLabel = new JLabel("ID:");
		payRecordPayHoursLabel = new JLabel("Pay Hours");
		payRecordPayRate = new JLabel("Pay Rate:");
		empRecStatsLabel = new JLabel("Current Employee Record and Stat(Total & Average Pays):");
		empIdText = new JTextField(5);
		empFirstNameText = new JTextField(15);
		empLastNameText = new JTextField(15);
		empStreetText = new JTextField(15);
		empHouseNumText = new JTextField(10);
		empCityText = new JTextField(15);
		empZipCodeText = new JTextField(8);
		payPeriodIdText = new JTextField(5);
		payPeriodStartDateText = new JTextField(12);
		payPeriodEndDateText = new JTextField(12);
		payRecordMonthIncomeText = new JTextField(12);
		payRecordNumMonthsText = new JTextField(12);
		payRecordIdText = new JTextField(4);
		payRecordPayHoursText = new JTextField(12);
		payRecordPayRateText = new JTextField(12);
		textArea = new JTextArea(10, 15);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		buttonGroup.add(fullTimeRadioButton);
		buttonGroup.add(hourlyRadioButton);

		jp = new JScrollPane(textArea);

		String[] statesList = { "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
				"Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas",
				"Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi",
				"Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York",
				"North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island",
				"South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington",
				"West Virginia", "Wisconsin", "Wyoming" };

		statesCombList = new JComboBox(statesList);
		statesCombList.setSelectedIndex(0);

		AddEmployeeButton = new JButton("Add Employee");
		AddPayRecButton = new JButton("Add Pay Record");
		CloseButton = new JButton("Close");

	}// end of creating objects method

	private void renderLayout() {
		// top (Employee) section Panels
		JPanel top = new JPanel();
		JPanel center = new JPanel();
		JPanel bottom = new JPanel();
		JPanel mid = new JPanel();
		JPanel mid2 = new JPanel();
		JPanel mid3 = new JPanel();
		JPanel mid4 = new JPanel();

		// Employee section- blue text
		mid2.setLayout(new GridLayout());
		mid2.add(empLabel, "East");
		mid2.add(empLabel).setForeground(Color.blue);
		mid2.setBorder(BorderFactory.createLineBorder(Color.black));

		// Employee section- blue text
		mid2.setLayout(new GridLayout());
		mid2.add(empLabel, "East");
		mid2.add(empLabel).setForeground(Color.blue);
		mid2.setBorder(BorderFactory.createLineBorder(Color.black));

		// Mid (Pay Period) Panels
		JPanel payTop = new JPanel();
		JPanel payBottom = new JPanel();

		// Mid bottom (Pay Record) Panels
		JPanel payRecTop = new JPanel();
		JPanel payRecBottom0 = new JPanel();
		JPanel payRecBottom = new JPanel();
		JPanel payRecBottom2 = new JPanel();
		JPanel payRecBottom3 = new JPanel();

		// Bottom Section (Current Employee Record)
		JPanel empRecStatsPanel = new JPanel();
		JPanel empRecStatsSPPanel = new JPanel();
		JPanel closePanel = new JPanel();

		// ID,FName,LName
		top.setLayout(new FlowLayout());
		top.add(empIdLabel);
		top.add(empIdText);
		top.add(empFirstNameLabel);
		top.add(empFirstNameText);
		top.add(empLastNameLabel);
		top.add(empLastNameText);

		// emp status and radio buttons
		center.setLayout(new FlowLayout());
		center.add(empStatusLabel);
		center.add(fullTimeRadioButton);
		center.add(hourlyRadioButton);

		// employee address section start
		mid3.setLayout(new GridLayout());
		mid3.add(empAddressLabel, "East");

		bottom.setLayout(new FlowLayout());
		// emp address , street, number, city
		bottom.add(empStreetLabel, "North");
		bottom.add(empStreetText, "North");
		bottom.add(empHouseNumLabel, "North");
		bottom.add(empHouseNumText, "North");
		bottom.add(empCityLabel, "North");
		bottom.add(empCityText, "North");

		// emp address state drop down, zipcode
		mid.add(empStateLabel, "Center");
		mid.add(statesCombList, "Center");
		mid.add(empZipCodeLabel);
		mid.add(empZipCodeText);
		// bottom.setLayout( new GridLayout());

		// Add Employee button
		mid4.setLayout(new FlowLayout());
		mid4.add(AddEmployeeButton, "South");

		// Pay Period section Start
		payTop.setLayout(new GridLayout());
		payTop.setBorder(BorderFactory.createLineBorder(Color.black));
		payTop.add(payPeriodLabel, "East");
		payTop.add(payPeriodLabel).setForeground(Color.blue);

		payBottom.setLayout(new FlowLayout());
		payBottom.add(payPeriodIdLabel);
		payBottom.add(payPeriodIdText);
		payBottom.add(payPeriodStartDateLabel);
		payBottom.add(payPeriodStartDateText);
		payBottom.add(payPeriodEndDateLabel);
		payBottom.add(payPeriodEndDateText);

		// Pay Record section Start
		payRecTop.setLayout(new GridLayout());
		// payRecTop.setBorder(BorderFactory.createLineBorder(Color.black));
		payRecTop.add(payRecordLabel, "East");
		payRecTop.add(payRecordLabel).setForeground(Color.blue);

		payRecBottom0.setLayout(new FlowLayout());
		payRecBottom0.add(payRecordIdLabel);
		payRecBottom0.add(payRecordIdText);

		// Pay Record Month Income, # of months
		payRecBottom.setLayout(new FlowLayout());
		payRecBottom.add(payRecordMonthIncomeLabel);
		payRecBottom.add(payRecordMonthIncomeText);
		payRecBottom.add(payRecordNumMonthsLabel);
		payRecBottom.add(payRecordNumMonthsText);

		// Pay Record pay hours, pay rate
		payRecBottom2.setLayout(new FlowLayout());
		payRecBottom2.add(payRecordPayHoursLabel);
		payRecBottom2.add(payRecordPayHoursText);
		payRecBottom2.add(payRecordPayRate);
		payRecBottom2.add(payRecordPayRateText);

		// Pay Record add pay record button
		payRecBottom3.setLayout(new FlowLayout());
		payRecBottom3.add(AddPayRecButton);

		// Bottom panes, Current Employee Record and Stat
		empRecStatsPanel.setLayout(new GridLayout());
		empRecStatsPanel.add(empRecStatsLabel, "East");
		empRecStatsPanel.add(empRecStatsLabel).setForeground(Color.blue);
		empRecStatsPanel.setBorder(BorderFactory.createLineBorder(Color.black));

		textArea.setLayout(new GridLayout(15, 15));
		empRecStatsSPPanel.setLayout(new GridLayout());
		empRecStatsSPPanel.add(jp);

		closePanel.setLayout(new FlowLayout());
		closePanel.add(CloseButton, "South");

		setLayout(new GridLayout(18, 3));
		setBorder(BorderFactory.createLineBorder(Color.green, 12));
		add(mid2, "East");
		add(top, "North");
		add(center, "Center");
		add(mid3, "Center");
		add(bottom, "South");
		add(mid, "Center");
		add(mid4, "Center");
		add(payTop, "Center");
		add(payBottom, "Center");
		add(payRecTop, "Center");
		add(payRecBottom0, "Center");
		add(payRecBottom, "Center");
		add(payRecBottom2, "Center");
		add(payRecBottom3, "Center");
		add(empRecStatsPanel);
		add(empRecStatsSPPanel);
		add(closePanel);
	}// end of Layout method

	void createEmployee() {
		try {
			empId = Integer.parseInt(empIdText.getText());
		} catch (NumberFormatException eie) {
			String errorMessage = "You entered an invalid value for employee ID: " + empIdText.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			empIdText.setText(null);
			empIdText.grabFocus();
			return;
		}

		try {
			empFirstName = empFirstNameText.getText();
			if (empFirstName.isEmpty()) {
				String errorMessage = "You entered an invalid value for first name: " + empFirstName;
				JOptionPane.showMessageDialog(null, errorMessage);
				empFirstNameText.setText(null);
				empFirstNameText.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for first name: " + empFirstName;
			JOptionPane.showMessageDialog(null, errorMessage);
			empFirstNameText.setText(null);
			empFirstNameText.grabFocus();
			return;
		}

		try {
			empLastName = empLastNameText.getText();
			if (empLastName.isEmpty()) {
				String errorMessage = "You entered an invalid value for last name: " + empLastName;
				JOptionPane.showMessageDialog(null, errorMessage);
				empLastNameText.setText(null);
				empLastNameText.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for last name: " + empLastName;
			JOptionPane.showMessageDialog(null, errorMessage);
			empLastNameText.setText(null);
			empLastNameText.grabFocus();
			return;
		}

		try {
			fullTimeRadioButton.setActionCommand("FULLTIME");
			hourlyRadioButton.setActionCommand("HOURLY");
			empStatus = buttonGroup.getSelection().getActionCommand();
		} catch (Exception e) {
			String errorMessage = "You must select employee status";
			JOptionPane.showMessageDialog(null, errorMessage);
			return;
		}

		try {
			empStreet = empStreetText.getText();
			if (empStreet.isEmpty()) {
				String errorMessage = "You entered an invalid value for street: " + empStreet;
				JOptionPane.showMessageDialog(null, errorMessage);
				empStreetText.setText(null);
				empStreetText.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for street: " + empStreet;
			JOptionPane.showMessageDialog(null, errorMessage);
			empStreetText.setText(null);
			empStreetText.grabFocus();
			return;
		}

		try {
			empHouseNum = Integer.parseInt(empHouseNumText.getText());
		} catch (NumberFormatException eie) {
			String errorMessage = "You entered an invalid value for house number: " + empHouseNumText.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			empHouseNumText.setText(null);
			empHouseNumText.grabFocus();
			return;
		}

		try {
			empCity = empCityText.getText();
			if (empCity.isEmpty()) {
				String errorMessage = "You entered an invalid value for city: " + empCity;
				JOptionPane.showMessageDialog(null, errorMessage);
				empCityText.setText(null);
				empCityText.grabFocus();
				return;
			}
		} catch (Exception lne) {
			String errorMessage = "You entered an invalid value for city: " + empCity;
			JOptionPane.showMessageDialog(null, errorMessage);
			empCityText.setText(null);
			empCityText.grabFocus();
			return;
		}

		empState = statesCombList.getSelectedItem().toString();

		try {
			empZipCode = Integer.parseInt(empZipCodeText.getText());
		} catch (NumberFormatException eie) {
			String errorMessage = "You entered an invalid value for zip code: " + empZipCodeText.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			empZipCodeText.setText(null);
			empZipCodeText.grabFocus();
			return;
		}

		employee = payRoll.createEmployee(empId, empStatus, empStreet, empHouseNum, empCity, empState, empZipCode,
				empFirstName, empLastName);
		empCount++;
		resetEmployeeFields();

	}

	private void resetEmployeeFields() {
		empIdText.setText(null);
		empIdText.requestFocus();
		empFirstNameText.setText(null);
		empLastNameText.setText(null);
		empStreetText.setText(null);
		empHouseNumText.setText(null);
		empCityText.setText(null);
		empZipCodeText.setText(null);
		payPeriodIdText.requestFocus();
	}

	void createPayRecord() throws Exception {
		try {
			payPeriodId = Integer.parseInt(payPeriodIdText.getText());
		} catch (NumberFormatException eie) {
			String errorMessage = "You entered an invalid value for period ID: " + payPeriodIdText.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			payPeriodIdText.setText(null);
			payPeriodIdText.grabFocus();
			return;
		}

		try {
			payPeriodStartDate = LocalDate.parse(payPeriodStartDateText.getText(),
					DateTimeFormatter.ofPattern("MM/dd/yyyy"));
		} catch (Exception e) {
			String errorMessage = "You entered an invalid value for pay period start date: "
					+ payPeriodStartDateText.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			payPeriodStartDateText.setText(null);
			payPeriodStartDateText.grabFocus();
			return;
		}

		try {
			payPeriodEndDate = LocalDate.parse(payPeriodEndDateText.getText(),
					DateTimeFormatter.ofPattern("MM/dd/yyyy"));

			// validate payroll period is at least one day
			long days = ChronoUnit.DAYS.between(payPeriodStartDate, payPeriodEndDate);
			if (days < 1) {
				String errorMessage = "Your pay roll period should be at least one day";
				JOptionPane.showMessageDialog(null, errorMessage);
				payPeriodEndDateText.setText(null);
				payPeriodEndDateText.grabFocus();
				return;
			}

		} catch (Exception e) {
			String errorMessage = "You entered an invalid value for pay period start date: "
					+ payPeriodEndDateText.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			payPeriodEndDateText.setText(null);
			payPeriodEndDateText.grabFocus();
			return;
		}

		try {
			payRecId = Integer.parseInt(payRecordIdText.getText());
		} catch (NumberFormatException eie) {
			String errorMessage = "You entered an invalid value for pay record ID: " + payRecordIdText.getText();
			JOptionPane.showMessageDialog(null, errorMessage);
			payRecordIdText.setText(null);
			payRecordIdText.grabFocus();
			return;
		}

		if (fullTimeRadioButton.isSelected()) {
			try {
				payRecMonthIncome = Double.parseDouble(payRecordMonthIncomeText.getText());
			} catch (NumberFormatException eie) {
				String errorMessage = "You entered an invalid value for monthly income: "
						+ payRecordMonthIncomeText.getText();
				JOptionPane.showMessageDialog(null, errorMessage);
				payRecordMonthIncomeText.setText(null);
				payRecordMonthIncomeText.grabFocus();
				return;
			}

			try {
				payRecNumMonths = Integer.parseInt(payRecordNumMonthsText.getText());
				long months = ChronoUnit.MONTHS.between(payPeriodStartDate, payPeriodEndDate);
				if (payRecNumMonths > months) {
					String errorMessage = "You exceeded the number of months based on your period dates: "
							+ payRecordNumMonthsText.getText();
					JOptionPane.showMessageDialog(null, errorMessage);
					payRecordNumMonthsText.setText(null);
					payRecordNumMonthsText.grabFocus();
					return;
				}
			} catch (NumberFormatException eie) {
				String errorMessage = "You entered an invalid value for number of months: "
						+ payRecordNumMonthsText.getText();
				JOptionPane.showMessageDialog(null, errorMessage);
				payRecordNumMonthsText.setText(null);
				payRecordNumMonthsText.grabFocus();
				return;
			}
		}

		if (hourlyRadioButton.isSelected()) {
			try {
				payRecPayHours = Double.parseDouble(payRecordPayHoursText.getText());
			} catch (NumberFormatException eie) {
				String errorMessage = "You entered an invalid value for pay hours: " + payRecordPayHoursText.getText();
				JOptionPane.showMessageDialog(null, errorMessage);
				payRecordPayHoursText.setText(null);
				payRecordPayHoursText.grabFocus();
				return;
			}

			try {
				payRecPayRate = Double.parseDouble(payRecordPayRateText.getText());
			} catch (NumberFormatException eie) {
				String errorMessage = "You entered an invalid value for pay rate: " + payRecordPayRateText.getText();
				JOptionPane.showMessageDialog(null, errorMessage);
				payRecordPayRateText.setText(null);
				payRecordPayRateText.grabFocus();
				return;
			}
		}

		payRoll.createPayRecord(employee, payRecId, payRecPayHours, payRecPayRate, payRecMonthIncome, payRecNumMonths,
				payPeriodId, payPeriodStartDate, payPeriodEndDate);
		payRecCount++;
		resetPayRecordFields();
		payRoll.displayPayRecord(textArea);
	}

	private void resetPayRecordFields() {
		payPeriodIdText.setText(null);
		payPeriodIdText.requestFocus();
		payPeriodStartDateText.setText(null);
		payPeriodEndDateText.setText(null);
		payRecordMonthIncomeText.setText(null);
		payRecordNumMonthsText.setText(null);
		payRecordIdText.setText(null);
		payRecordPayHoursText.setText(null);
		payRecordPayRateText.setText(null);
	}

	void closeApplication() throws Exception {
		payRoll.writeToFile();
		System.exit(0);
	}

	public static void main(String[] args) {
		try {
			displayMainUI();
		} catch (Exception ex) {
			String message = "An error ocurred during program execution - " + ex.getMessage();
			JOptionPane.showMessageDialog(null, message);
		}
		boolean continueInput = true;
		do {
			numEmp = JOptionPane
					.showInputDialog("Enter the number of employees you want to process through the User Interface");
			try {
				n = Integer.parseInt(numEmp);
				if (n == 0) {
					String message = "Number of employees can't be zero";
					JOptionPane.showMessageDialog(null, message);
				} else {
					continueInput = false;
				}
			} catch (NumberFormatException e) {
				String message = "Try again. (" + "Incorrect input: an integer is required)";
				JOptionPane.showMessageDialog(null, message);
			}
		} while (continueInput);

		try {
			processPayRollFile();
		} catch (Exception e) {
			String message = "An error ocurred during program execution - " + e.getMessage();
			JOptionPane.showMessageDialog(null, message);
		}
	}

	private static void displayMainUI() throws Exception {
		JFrame f = new JFrame("Pay Roll System");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container contentPane = f.getContentPane();
		contentPane.add(new UserGUI());
		f.pack();
		f.setVisible(true);
		f.setSize(1000, 1000);
	}
}
